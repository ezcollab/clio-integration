package com.easycollab.integration.clio.ringcentral.service;

import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.easycollab.integration.clio.ringcentral.dto.CUser;
import com.easycollab.integration.clio.ringcentral.dto.ClioData;
import com.easycollab.integration.clio.ringcentral.dto.ClioResponse;
import com.easycollab.integration.clio.ringcentral.dto.EmailAddress;
import com.easycollab.integration.clio.ringcentral.dto.PhoneNumber;
import com.easycollab.integration.clio.ringcentral.dto.WebSite;
import com.easycollab.integration.clio.ringcentral.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ringcentral.definitions.ContactList;
import com.ringcentral.definitions.PersonalContactRequest;
import com.ringcentral.definitions.PersonalContactResource;


@Service(Constants.CLIO_SERVICE)
public class ClioService {

	public static final Logger logger = Logger.getLogger(ClioService.class);

	@Autowired
	private RingCentralService ringCentralService;

	public String listContacts(String authToken) throws Exception {
		logger.info("Retrieiving list Of Contacts from Clio");
		return callRestForClio(getRestUrl("contacts.json"), null,
				HttpMethod.GET, Constants.CONTACTS_ATTRS, authToken);
	}

	public String getRestUrl(String action) {
		return Constants.CLIO_REST_BASE + action;
	}

	public <T> T callRestForClio(String url, JSONObject data,
			HttpMethod method, List<String> attrs, String authToken)
			throws Exception {
		try {
			logger.info("callRestForClio Method started");
			String request = null;
			if (data != null) {
				JSONObject wrapper = new JSONObject();
				wrapper.put("data", data);
				request = wrapper.toString();
			}
			RestTemplate restTemplate = new RestTemplate(
					new HttpComponentsClientHttpRequestFactory());
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", "Bearer " + authToken);
			LinkedMultiValueMap<String, String> allRequestParams = new LinkedMultiValueMap<String, String>();
			if (attrs != null) {
				allRequestParams.put("fields", attrs);
			}

			UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(
					url).queryParams(allRequestParams);
			UriComponents uriComponents = builder.build().encode();
			HttpEntity<String> entity = new HttpEntity<String>(request, headers);

			logger.debug("The uri is: " + uriComponents.toUri());
			logger.debug("The header is: " + headers);
			logger.debug("The request is: " + request);

			ResponseEntity<String> responseEntity = restTemplate.exchange(
					uriComponents.toUri(), method, entity, String.class);

			logger.info("callRestForClio Method Ended");
			return (T) responseEntity.getBody();
		} catch (HttpClientErrorException e) {
			logger.error("error in CallRestForClio " + e);
			throw e;
		}

	}

	public void createContatcts(CUser cuser) throws Exception {
		
		logger.info("Create Contacts Method Started");
		String jsonArray = listContacts(Constants.CLIO_ACCESS_TOKEN);
		Gson gson = new GsonBuilder().create();
		ClioResponse response = gson.fromJson(jsonArray, ClioResponse.class);
		ContactList contactList	 = ringCentralService.getContactList(cuser);
		PersonalContactResource[] contactRecords=contactList.records;
		List<PersonalContactResource> contacts =Arrays.asList(contactRecords);
		if (!ObjectUtils.isEmpty(response.getData())) {
			logger.info("Clio Contact size " + response.getData().size());
			if (!ObjectUtils.isEmpty(contacts)) {
				response.getData().stream().forEach(data -> {
					
									PersonalContactRequest contact = convertClioDataToContact(data);
									PersonalContactResource exstContact = contacts.stream().filter(predicate -> 
									(!StringUtils.isEmpty(predicate.mobilePhone) && predicate.mobilePhone.equalsIgnoreCase(contact.mobilePhone))||
									(!StringUtils.isEmpty(predicate.email) && predicate.email.equalsIgnoreCase(contact.email))).findAny().orElse(null);
									if (ObjectUtils.isEmpty(exstContact)) {
										logger.info("Creating new Contact ");
										ringCentralService.createContact(contact,cuser);
									} else {
										logger.info("Updating Exsting Contact "+ exstContact.id);
										PersonalContactRequest updateRequest=convertResourceToRequest(exstContact);
										ringCentralService.updateContact(updateRequest,cuser,String.valueOf(exstContact.id));
									}
									
								});
					}
		}
		
		logger.info("Create Contacts Method Ended");
	}

	public PersonalContactRequest convertClioDataToContact(ClioData clioData) {

		PersonalContactRequest contact = new PersonalContactRequest();

		contact.email(clioData.getPrimary_email_address());
		// contact.setCompany("Example, Inc.");
		// contact.setNotes("#1 Customer");
		contact.firstName(clioData.getFirst_name());
		contact.lastName(clioData.getLast_name());
		// contact.setJobTitle("CEO");

		List<WebSite> lisOfWebsites = clioData.getWeb_sites();
		String websiteStr = null;
		if (!ObjectUtils.isEmpty(lisOfWebsites)) {
			for (WebSite website : lisOfWebsites) {
				if (websiteStr == null) {
					websiteStr = website.getAddress();
				} else {
					websiteStr = websiteStr + "," + website.getAddress();
				}
			}
		}
		contact.webPage(websiteStr);
		contact.mobilePhone(clioData.getPrimary_phone_number());
		List<EmailAddress> emailAddresses = clioData.getEmail_addresses();
		emailAddresses.stream().forEach(email -> {
			if (contact.email2 == null) {
				contact.email2(email.getAddress());
			} else if (contact.email3 == null) {
				contact.email3(email.getAddress());
			}
		});
		List<PhoneNumber> phoneNumbers = clioData.getPhone_numbers();
		phoneNumbers.stream().forEach(phone -> {
			if (phone.getName().contains("business")) {
				if (contact.businessPhone == null)
					contact.businessPhone(phone.getNumber());
				else if (contact.businessPhone2 == null) {
					contact.businessPhone2(phone.getNumber());
				}
			} else {
				if (contact.homePhone == null)
					contact.homePhone(phone.getNumber());
				else if (contact.homePhone2 == null)
					contact.homePhone2(phone.getNumber());
			}
		});

		return contact;
	}
	
	
	public PersonalContactRequest convertResourceToRequest(PersonalContactResource resource){
		PersonalContactRequest request=new PersonalContactRequest();
		request.assistantPhone(resource.assistantPhone);
		request.birthday(resource.birthday);
		request.businessAddress(resource.businessAddress);
		request.businessFax(resource.businessFax);
		request.businessPhone(resource.businessPhone);
		request.businessPhone2(resource.businessPhone2);
		request.callbackPhone(resource.callbackPhone);
		request.carPhone(resource.carPhone);
		request.company(resource.company);
		request.companyPhone(resource.companyPhone);
		request.email(resource.email);
		request.email2(resource.email2);
		request.email3(resource.email3);
		request.firstName(resource.firstName);
		request.homeAddress(resource.homeAddress);
		request.homePhone(resource.homePhone);
		request.homePhone2(resource.homePhone2);
		request.jobTitle(resource.jobTitle);
		request.lastName(resource.lastName);
		request.middleName(resource.middleName);
		request.mobilePhone(resource.mobilePhone);
		request.nickName(resource.nickName);
		request.notes(resource.notes);
		request.otherAddress(resource.otherAddress);
		request.otherFax(resource.otherFax);
		request.otherPhone(resource.otherPhone);
		request.webPage(resource.webPage);

		return request;
		
	}

}
