package com.easycollab.integration.clio.ringcentral.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.easycollab.integration.clio.ringcentral.dto.CUser;
import com.easycollab.integration.clio.ringcentral.service.ClioService;
@RestController
public class IntegrationController {

	@Autowired
	private ClioService clioService;

	@GetMapping("/createContact")
	public ResponseEntity<String> createContacts() {
		try {
			CUser cuser =new CUser();
			clioService.createContatcts(cuser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}

}
