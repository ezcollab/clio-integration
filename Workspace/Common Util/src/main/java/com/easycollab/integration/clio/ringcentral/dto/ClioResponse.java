package com.easycollab.integration.clio.ringcentral.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClioResponse {

	private List<ClioData> data;

	public List<ClioData> getData() {
		return data;
	}

	public void setData(List<ClioData> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "ClioResponse [data=" + data + "]";
	}

}
