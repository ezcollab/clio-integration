package com.easycollab.integration.clio.ringcentral.dto;

import java.util.List;


public class ClioData {

	private Integer id;
	private String type;
	private String last_name;
	private String first_name;
	private String primary_email_address;
	private String primary_phone_number;
	private List<CustomFieldValue> custom_field_values;
	private List<EmailAddress> email_addresses;
	private List<PhoneNumber> phone_numbers;
	private List<WebSite> web_sites;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getPrimary_email_address() {
		return primary_email_address;
	}

	public void setPrimary_email_address(String primary_email_address) {
		this.primary_email_address = primary_email_address;
	}

	public String getPrimary_phone_number() {
		return primary_phone_number;
	}

	public void setPrimary_phone_number(String primary_phone_number) {
		this.primary_phone_number = primary_phone_number;
	}

	public List<CustomFieldValue> getCustom_field_values() {
		return custom_field_values;
	}

	public void setCustom_field_values(
			List<CustomFieldValue> custom_field_values) {
		this.custom_field_values = custom_field_values;
	}

	public List<EmailAddress> getEmail_addresses() {
		return email_addresses;
	}

	public void setEmail_addresses(List<EmailAddress> email_addresses) {
		this.email_addresses = email_addresses;
	}

	public List<PhoneNumber> getPhone_numbers() {
		return phone_numbers;
	}

	public void setPhone_numbers(List<PhoneNumber> phone_numbers) {
		this.phone_numbers = phone_numbers;
	}

	public List<WebSite> getWeb_sites() {
		return web_sites;
	}

	public void setWeb_sites(List<WebSite> web_sites) {
		this.web_sites = web_sites;
	}
}
