package com.easycollab.integration.clio.ringcentral.dto;

public class CUser {

	private String ringCentralClientId = "saiS11QdQ5-n9UciTs7XHw";
	private String ringCentralClientSecret = "wZcD_LzhQUKM53V4j42bGw8a7PaJ_-StWF9wt3qzgYhg";
	private String ringCentralServer = "https://platform.devtest.ringcentral.com";
	private String ringCentralAccessToken = "U0pDMTJQMDFQQVMwMHxBQUN4VVJLVTJQV0w3X2p6eDI3NDR0OEp3dk9wRjV3ck9heThtN3RGR3NGTG1icGZ0dEhRU1p2bXZkRHpaSGwzbTRoNF9QWWRPMEVYNENYQjd4dmJsWHJoWFhOZjR4Nk9MczBqZ0QzUEtLZnMxSi16cGdVSklFSlMyMEx1NmRkR051UkZiYVBJSnNQOHZkMXpDbjVMeEl5amFmalZkMWVkQmtZcENRbDdKNmRua0tDbGxKVUdOV0Rxa181dVRqTExWeXdnWlhrd3BfR0JrWTR6N1M1WEZVZkJ8QnppXzhnfEdBRXhKc09xXzBNdmt1RFNHV2hQeXd8QUE";
	private String ringCentralRefreshToken = "U0pDMTJQMDFQQVMwMHxBQUN4VVJLVTJQV0w3X2p6eDI3NDR0OEp3dk9wRjV3ck9heThtN3RGR3NGTG1icGZ0dEhRU1p2bXZkRHpaSGwzbTRoNF9QWWRPMEVYNENYQjd4dmJsWHJoOFF5bW16LXpRc0VqZ0QzUEtLZnMxTW5nbGtFODVMMmoyMEx1NmRkR051UkZiYVBJSnNQOHZkMXpDbjVMeEl5amFmalZkMWVkQmtZcENRbDdKNmRua0tDbGxKVUdOV0Rxa181dVRqTExWeXdsV1JBeWswVVpFZS14WV95M3dMUlV8QnppXzhnfG9meDRBXzBTMFVWTFFqYnhCY1BVTXd8QUE";

	public String getRingCentralClientId() {
		return ringCentralClientId;
	}

	public void setRingCentralClientId(String ringCentralClientId) {
		this.ringCentralClientId = ringCentralClientId;
	}

	public String getRingCentralClientSecret() {
		return ringCentralClientSecret;
	}

	public void setRingCentralClientSecret(String ringCentralClientSecret) {
		this.ringCentralClientSecret = ringCentralClientSecret;
	}

	public String getRingCentralServer() {
		return ringCentralServer;
	}

	public void setRingCentralServer(String ringCentralServer) {
		this.ringCentralServer = ringCentralServer;
	}

	public String getRingCentralAccessToken() {
		return ringCentralAccessToken;
	}

	public void setRingCentralAccessToken(String ringCentralAccessToken) {
		this.ringCentralAccessToken = ringCentralAccessToken;
	}

	public String getRingCentralRefreshToken() {
		return ringCentralRefreshToken;
	}

	public void setRingCentralRefreshToken(String ringCentralRefreshToken) {
		this.ringCentralRefreshToken = ringCentralRefreshToken;
	}

}
