package com.easycollab.integration.clio.ringcentral.utils;

import java.util.Arrays;
import java.util.List;

public class Constants {
	
	//Annotation Constants
	
	public static final String CLIO_SERVICE="clioService";
	public static final String RING_CENTRAL_SERVICE="ringCentralService";
	
	
	//Clio Constants 
	
	public static final String CLIO_REST_BASE = "https://app.clio.com/api/v4/";

	public static final String CLIO_ACCESS_TOKEN = "vjuFB6PrjAoiGEtxDnAb5YN4MUFgFRV2PdRVeUg9";
	
	public static final List<String> CONTACTS_ATTRS = Arrays
			.asList("id,name,type,last_name,first_name,primary_email_address,primary_phone_number,custom_field_values{field_name,value,field_type},email_addresses{address,name},phone_numbers{number,name},web_sites{address,name}");


}
