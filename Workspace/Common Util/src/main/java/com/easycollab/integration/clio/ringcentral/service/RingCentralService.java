package com.easycollab.integration.clio.ringcentral.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.easycollab.integration.clio.ringcentral.dto.CUser;
import com.easycollab.integration.clio.ringcentral.utils.Constants;
import com.ringcentral.RestClient;
import com.ringcentral.definitions.ContactList;
import com.ringcentral.definitions.PersonalContactRequest;
import com.ringcentral.definitions.PersonalContactResource;
import com.ringcentral.definitions.TokenInfo;
import com.ringcentral.paths.restapi.account.extension.addressbook.Index;

@Service(Constants.RING_CENTRAL_SERVICE)
public class RingCentralService {

	private static final Logger logger = Logger
			.getLogger(RingCentralService.class);

	@Value("${header.auth.token}")
	private String authToken;

	public void createContact(PersonalContactRequest contact,CUser cuser) {
		logger.info("createRingCentralContact method started ");
		Index index=getAddressBook(cuser);
		index.contact().post(contact);
		logger.info("createRingCentralContact method Ended ");
	}

	public ContactList getContactList(CUser cuser) {
		logger.info("Getting Contact List From ring Central");
		Index index=getAddressBook(cuser);
		return index.contact().list();
	}

	public void updateContact(PersonalContactRequest contact,CUser cuser,String contactId) {
		logger.info("Updating Exsting Contact Started ");
		Index index=getAddressBook(cuser);
		index.contact(contactId).put(contact);
		logger.info("Updating Exsting Contact Ended");
	}
	
	
	
	public Index getAddressBook(CUser cuser){
		RestClient restClient =getRestClient(cuser);
		return restClient.restapi().account().extension().addressbook();
	}

	public RestClient getRestClient(CUser cuser) {
		RestClient restClient = null;
		try {
			String ringcentral_client_id = cuser.getRingCentralClientId();
			String ringcentral_client_secret = cuser
					.getRingCentralClientSecret();
			String ringcentral_server = cuser.getRingCentralServer();

			restClient = new RestClient(ringcentral_client_id,
					ringcentral_client_secret, ringcentral_server);
			TokenInfo ti = new TokenInfo();
			ti.token_type("bearer");
			ti.refresh_token(cuser.getRingCentralRefreshToken());
			ti.access_token(cuser.getRingCentralAccessToken());
			restClient.token = ti;
			//restClient.refresh();
			cuser.setRingCentralAccessToken(restClient.token.access_token);
			cuser.setRingCentralRefreshToken(restClient.token.refresh_token);
			System.out.println("token.access_token ==> "
					+ restClient.token.access_token);
			System.out.println("restClient.token.refresh_token ==> "
					+ restClient.token.refresh_token);

		} catch (Exception e) {
			logger.error("Error in building rest client ", e);
		}
		return restClient;
	}
	
	public static void main(String[] args) {
		
		RingCentralService service=new RingCentralService();
		RestClient restClient =service.getRestClient(new CUser());
		Index index =restClient.restapi().account().extension().addressbook();
		ContactList contactList =index.contact().list();
		PersonalContactResource[] contactRecords=contactList.records;
		
	}

}
